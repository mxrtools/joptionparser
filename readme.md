JOptionParser
==============

Simple Java command line option parser.

Usage
=====

		OptionParser optionParser = new OptionParser();

        Option fileName = new Option("f", "file", "File to be used by the application", Option.Mandatory.HAS_VALUE, Option.Type.STRING );
        ChoiceOption choiceOption = new ChoiceOption("c", "choice", "Choices to be used", new String[]{"a", "b", "c"});
        Option optionWithoutValue = new Option("v", "verbose", "Run all in verbose mode", Option.Mandatory.NO_VALUE, Option.Type.STRING );

        optionParser.registerOption(fileName);
        optionParser.registerOption(choiceOption);
        optionParser.registerOption(optionWithoutValue);

        try {
            optionParser.parseOptions(args);
            if (optionParser.isHelpRequested()){
                System.out.println(optionParser.getHelpMessage());
                System.exit(0);
			}
            if (optionParser.isOptionProvided("f"))
                System.out.println("File parameter:" + optionParser.getOptionValue("f"));
            if (optionParser.isOptionProvided("c"))
                System.out.println("Choice parameter:" + optionParser.getOptionValue("c"));
            if (optionParser.isOptionProvided("v"))
                    System.out.println("Verbose option was set");
        } catch (OptionParseException e) {
            e.printStackTrace();
        }

Application start:
    java ...  -f file_name -c a -v
or 
    java ... --file file_name --choice a --verbose